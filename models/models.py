# -*- coding: utf-8 -*-

from odoo import models, fields, api, exceptions


MAP_INVOICE_TYPE_PARTNER_TYPE = {
    'out_invoice': 'customer',
    'out_refund': 'customer',
    'in_invoice': 'supplier',
    'in_refund': 'supplier',
}
# Since invoice amounts are unsigned, this is how we know if money comes in or goes out
MAP_INVOICE_TYPE_PAYMENT_SIGN = {
    'out_invoice': 1,
    'in_refund': 1,
    'in_invoice': -1,
    'out_refund': -1,
}

class account_register_massive_payments(models.TransientModel):
    _name = "account.register.massive.payments"
    _description = "Create payments for multiple invoices"

    payment_date = fields.Date(string='Payment Date', default=fields.Date.context_today, required=True, copy=False)
    journal_id = fields.Many2one('account.journal', string='Payment Journal', required=True, domain=[('type', 'in', ('bank', 'cash'))])
    payment_method_id = fields.Many2one('account.payment.method', string='Payment Method Type', required=True, oldname="payment_method")

    def _default_account_invoice_ids(self):
        return self.env['account.invoice'].browse(self._context.get('active_ids'))

    account_invoice_ids = fields.Many2many('account.invoice', default=_default_account_invoice_ids)

    def get_payment_vals(self, invoices):
        context = dict(self._context or {})
        active_model = context.get('active_model')
        active_ids = context.get('active_ids')

        # Checks on context parameters
        if not active_model or not active_ids:
            raise UserError(_("Programmation error: wizard action executed without active_model or active_ids in context."))
        if active_model != 'account.invoice':
            raise UserError(_("Programmation error: the expected model for this action is 'account.invoice'. The provided one is '%d'.") % active_model)

        # Checks on received invoice records
        if any(invoice.state != 'open' for invoice in invoices):
            raise UserError(_("You can only register payments for open invoices"))
        if any(inv.commercial_partner_id != invoices[0].commercial_partner_id for inv in invoices):
            raise UserError(_("In order to pay multiple invoices at once, they must belong to the same commercial partner."))
        if any(MAP_INVOICE_TYPE_PARTNER_TYPE[inv.type] != MAP_INVOICE_TYPE_PARTNER_TYPE[invoices[0].type] for inv in invoices):
            raise UserError(_("You cannot mix customer invoices and vendor bills in a single payment."))
        if any(inv.currency_id != invoices[0].currency_id for inv in invoices):
            raise UserError(_("In order to pay multiple invoices at once, they must use the same currency."))

        total_amount = sum(inv.residual * MAP_INVOICE_TYPE_PAYMENT_SIGN[inv.type] for inv in invoices)
        communication = ' '#.join([ref for ref in invoices.mapped('reference') if ref]) # need to solve this

        return {
            'journal_id': self.journal_id.id,
            'payment_method_id': self.payment_method_id.id,
            'payment_date': self.payment_date,
            'communication': communication,
            'invoice_ids': [(4, inv.id, None) for inv in invoices],
            'payment_type': total_amount > 0 and 'inbound' or 'outbound',
            'amount': abs(total_amount),
            'currency_id': invoices[0].currency_id.id,
            'partner_id': invoices[0].partner_id.id,
            'partner_type': MAP_INVOICE_TYPE_PARTNER_TYPE[invoices[0].type],
        }

    def create_payments(self):
        invoices_grouped_by_partner = {}
        for inv in self.account_invoice_ids:
            if not inv.partner_id.id in invoices_grouped_by_partner:
                invoices_grouped_by_partner[inv.partner_id.id] = [inv]
            else:
                invoices_grouped_by_partner[inv.partner_id.id].append(inv)

        for partner_id, invoices in invoices_grouped_by_partner.items():
            payment = self.env['account.payment'].create(self.get_payment_vals(invoices))
            payment.post()

        return {'type': 'ir.actions.act_window_close'}
